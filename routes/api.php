<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
|   Usually i group routes based on different modules or domains.
*/
Route::middleware('auth:api')->group(function (\Illuminate\Routing\Router $route) {

    Route::apiResource('users', UserController::class);

});
