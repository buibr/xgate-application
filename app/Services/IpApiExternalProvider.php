<?php

namespace App\Services;

use App\Services\Contracts\ExternalProviderInterface;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

class IpApiExternalProvider implements ExternalProviderInterface
{
    protected Response $request;

    public function request(array $options = [])
    {
        $this->request = Http::get('ip-api.com');

        return $this;
    }

    public function toArray()
    {
        if(!isset($this->request)){
            return null;
        }

        return json_decode($this->request->body(), true);
    }
}
