<?php

namespace App\Http\Requests;

use App\Enums\UserRoleEnum;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;

class UserStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            "name" => ['string'],
            "email" => ['required', 'email', Rule::unique(User::class, 'email')],
            "password" => ['required', Password::min(7)],
            "phone_number" => ['regex:/^([0-9\s\-\+\(\)]*)$/'],
            "api_key" => ['string'],
            "role" => ['string', Rule::in(array_map(fn($i) => $i->name, UserRoleEnum::cases()))],
        ];
    }
}
