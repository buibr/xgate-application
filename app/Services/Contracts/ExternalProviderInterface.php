<?php

namespace App\Services\Contracts;

interface ExternalProviderInterface
{
    public function request(array $options =[]);

    public function toArray();
}
