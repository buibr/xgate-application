<?php

namespace Tests\Feature;

use App\Enums\UserRoleEnum;
use App\Models\User;
use App\Services\Contracts\ExternalProviderInterface;
use App\Services\IpApiExternalProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;
use Tests\TestCase;
use function PHPUnit\Framework\assertCount;

class AdminFeatureTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->instance(
            ExternalProviderInterface::class,
            Mockery::mock(ExternalProviderInterface::class, function (Mockery\MockInterface $mock) {
                $mock->shouldReceive('request')->with([])->andReturn(new IpApiExternalProvider());
            }));

        $this->user = User::factory()->create([
            'name' => 'Administrator',
            'api_key' => '1234',
            'role' => UserRoleEnum::ADMIN->name
        ]);


        $this->actingAs($this->user);
    }

    /**
     * A basic test example.
     */
    public function test_admin_can_list_users(): void
    {
        User::factory(4)->create(); // one is created as admin.

        $response = $this->get(route('users.index'), [
            'X-API-KEY' => $this->user->api_key
        ]);

        $response->assertStatus(200);
        $response->assertJsonIsArray('data');

        assertCount(5, $response->json('data'));
    }

    public function test_admin_can_create_user(): void
    {
        $userData = User::factory()->make(['phone_number' => '+389 71-789-062'])->toArray();
        $userData['password'] = '1234567';

        $response = $this->post(route('users.store'), $userData, [
            'X-API-KEY' => $this->user->api_key
        ]);
        $response->assertCreated();
        $response->assertJson(['success' => 'true']);
    }

    public function test_admin_can_see_user()
    {
        $user = User::factory()->create();

        $response = $this->get(route('users.show', ['user' => $user->id]), [
            'X-API-KEY' => $this->user->api_key
        ]);
        $response->assertOk();
        $response->assertJson(['user' => $user->toArray()]);
    }


    public function test_admin_can_update_user()
    {

        $user = User::factory()->create();

        $name = 'New Name';

        $response = $this->put(route('users.update', ['user' => $user->id]), ['name' => $name], [
            'X-API-KEY' => $this->user->api_key
        ]);
        $response->assertOk();
        $response->assertJson(['success' => true]);
        $response->assertJson(['user' => ['name' => $name]]);
    }

    public function test_admin_can_delete_user()
    {
        $user = User::factory()->create();

        $response = $this->delete(route('users.update', ['user' => $user->id]), [], [
            'X-API-KEY' => $this->user->api_key
        ]);
        $response->assertOk();
        $response->assertJson(['success' => true]);
        $response->assertJson(['id' => $user->id]);
    }
}
