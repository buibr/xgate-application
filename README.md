
# Burhan Ibrahimi - Application

---

### Requirements and Instructions
- Docker
- PHP 8.2

---

### Power Up

Install dependencies
```bash
composer install
```

Create instances
```bash
./vendor/bin/sail up -d
```

---

### Testing

```bash
./vendor/bin/sail artisan test
```
