<?php

namespace App\Observers;

use App\Models\User;
use App\Services\Contracts\ExternalProviderInterface;
use Illuminate\Support\Facades\Http;

class UserObserver
{
    /**
     * Handle the User "created" event.
     */
    public function created(User $user): void
    {
        $ipapi = app(ExternalProviderInterface::class)->request([]);

        // we can update user with ip provided....
    }

    /**
     * Handle the User "updated" event.
     */
    public function updated(User $user): void
    {
        //
    }

    /**
     * Handle the User "deleted" event.
     */
    public function deleted(User $user): void
    {
        //
    }

    /**
     * Handle the User "restored" event.
     */
    public function restored(User $user): void
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     */
    public function forceDeleted(User $user): void
    {
        //
    }
}
