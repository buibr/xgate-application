<?php

namespace App\Enums;

enum UserRoleEnum
{
    case ADMIN;
    case USER;
}
